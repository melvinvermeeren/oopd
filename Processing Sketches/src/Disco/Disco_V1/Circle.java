/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package Disco.Disco_V1;

import processing.core.PApplet;

/**
 * Created by Melvin on 14-Feb-2014 13:20.
 */
class Circle
{
	final static private boolean isSuperFluidDrawingSystemEnabled = true;
	final static private int
			SPEEDX_MIN = -5,
			SPEEDX_MAX = 5,
			SPEEDY_MIN = -3,
			SPEEDY_MAX = 3,
			SPEEDX_CHANGE_MIN = -2,
			SPEEDX_CHANGE_MAX = 2,
			SPEEDY_CHANGE_MIN = -2,
			SPEEDY_CHANGE_MAX = 2;

	private PApplet parent;
	private int
			x,
			y,
			diameter,
			speedX = RandomInt.randomInt(SPEEDX_MIN, SPEEDX_MAX),
			speedY = RandomInt.randomInt(SPEEDY_MIN, SPEEDY_MAX),
			color;

	Circle(PApplet parent, int diameter, int color)
	{
		this.parent = parent;
		reposition();
		this.diameter = diameter;
		this.color = color;
	}

	void update()
	{
		checkCollision();
		draw();

		if (!isSuperFluidDrawingSystemEnabled)
		{
			x += speedX;
			y += speedY;
		}

		speedX += RandomInt.randomInt(SPEEDX_CHANGE_MIN, SPEEDX_CHANGE_MAX);
		speedY += RandomInt.randomInt(SPEEDY_CHANGE_MIN, SPEEDY_CHANGE_MAX);
		processing.core.PApplet.constrain(speedX, SPEEDX_MIN, SPEEDX_MAX);
		processing.core.PApplet.constrain(speedY, SPEEDY_MIN, SPEEDY_MAX);
	}

	void draw()
	{
		parent.fill(color);
		if (!isSuperFluidDrawingSystemEnabled) parent.ellipse(x, y, diameter, diameter);

		else /* Super fluid drawing system. */
		{
			/* Chance of repositioning. */
			if (RandomInt.randomInt(0, 20) == 10) reposition();

			/* Determine fastest moving direction. */
			int speedXtemp;
			int speedYtemp;
			boolean isSpeedXBiggest = true;

			if (speedX < 0) speedXtemp = -speedX;
			else if (speedX > 0) speedXtemp = speedX;
			else speedXtemp = 1;
			if (speedY < 0) speedYtemp = -speedY;
			else if (speedY > 0) speedYtemp = speedY;
			else speedYtemp = 1;

			if (speedYtemp > speedXtemp) isSpeedXBiggest = false;

			/* Calculate increments. */
			final float xIncrement;
			final float yIncrement;


			if (isSpeedXBiggest)
			{
				if (speedX < 0) xIncrement = -1;
				else xIncrement = 1;

				yIncrement = speedY / speedXtemp;

				float yCounter = 0;
				int lastY = 0;
				for (int xCounter = 0; xCounter < speedX; xCounter += xIncrement)
				{
					x += java.lang.Math.round(xIncrement);
					yCounter += yIncrement;
					if (speedY >= 0 && java.lang.Math.round(yCounter) > lastY)
					{
						y++;
						lastY = java.lang.Math.round(yCounter);
					} else if (speedY < 0 && java.lang.Math.round(yCounter) < lastY)
					{
						y--;
						lastY = java.lang.Math.round(yCounter);
					}

					parent.ellipse(x, y, diameter, diameter);
				}
			} else
			{
				if (speedY < 0) yIncrement = -1;
				else yIncrement = 1;

				xIncrement = speedX / speedYtemp;

				float xCounter = 0;
				int lastX = 0;
				for (int yCounter = 0; yCounter < speedY; yCounter += yIncrement)
				{
					y += java.lang.Math.round(yIncrement);
					xCounter += xIncrement;
					if (speedX >= 0 && java.lang.Math.round(xCounter) > lastX)
					{
						x++;
						lastX = java.lang.Math.round(xCounter);
					} else if (speedX < 0 && java.lang.Math.round(xCounter) > lastX)
					{
						x--;
						lastX = java.lang.Math.round(xCounter);
					}

					parent.ellipse(x, y, diameter, diameter);
				}
			}
		}
	}

	private void checkCollision()
	{
		if ((speedX < 0 && x + speedX < 0) || (speedX > 0 && x + speedX > parent.width - diameter)) speedX = -speedX;
		if ((speedY < 0 && y + speedY < 0) || (speedY > 0 && y + speedX > parent.height - diameter)) speedY = -speedY;
	}

	private void reposition()
	{
		x = RandomInt.randomInt(0, parent.width - diameter);
		y = RandomInt.randomInt(0, parent.height - diameter);
	}
}
