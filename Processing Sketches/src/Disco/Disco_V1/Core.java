/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package Disco.Disco_V1;

import processing.core.PApplet;

/**
 * Created by Melvin on 14-Feb-2014 11:47.
 */
public class Core extends PApplet
{
	public static void main(String args[])
	{
		PApplet.main(new String[]{"--present", "Disco.Disco_V1.Core"});
	}

	Circle[] circles;

	public void setup()
	{
		size(displayWidth, displayHeight);
		ellipseMode(CORNER);
		frameRate(30);
		background(0xFF000000);

		circles = new Circle[]
				{
						new Circle(this, 25, 0xFFFF0000),
						new Circle(this, 25, 0xFF00FF00),
						new Circle(this, 25, 0xFF0000FF),
						new Circle(this, 25, 0xFFFFFF00),
						new Circle(this, 25, 0xFFFF00FF),
						new Circle(this, 25, 0xFF00FFFF),
						new Circle(this, 25, 0xFFFF0000),
						new Circle(this, 25, 0xFF00FF00),
						new Circle(this, 25, 0xFF0000FF),
						new Circle(this, 25, 0xFFFFFF00),
						new Circle(this, 25, 0xFFFF00FF),
						new Circle(this, 25, 0xFF00FFFF),
						new Circle(this, 25, 0xFFFF0000),
						new Circle(this, 25, 0xFF00FF00),
						new Circle(this, 25, 0xFF0000FF),
						new Circle(this, 25, 0xFFFFFF00),
						new Circle(this, 25, 0xFFFF00FF),
						new Circle(this, 25, 0xFF00FFFF),
						new Circle(this, 25, 0xFFFF0000),
						new Circle(this, 25, 0xFF00FF00),
						new Circle(this, 25, 0xFF0000FF),
						new Circle(this, 25, 0xFFFFFF00),
						new Circle(this, 25, 0xFFFF00FF),
						new Circle(this, 25, 0xFF00FFFF),
						new Circle(this, 25, 0xFFFF0000),
						new Circle(this, 25, 0xFF00FF00),
						new Circle(this, 25, 0xFF0000FF),
						new Circle(this, 25, 0xFFFFFF00),
						new Circle(this, 25, 0xFFFF00FF),
						new Circle(this, 25, 0xFF00FFFF),
						new Circle(this, 25, 0xFFFF0000),
						new Circle(this, 25, 0xFF00FF00),
						new Circle(this, 25, 0xFF0000FF),
						new Circle(this, 25, 0xFFFFFF00),
						new Circle(this, 25, 0xFFFF00FF),
						new Circle(this, 25, 0xFF00FFFF),
						new Circle(this, 25, 0xFFFF0000),
						new Circle(this, 25, 0xFF00FF00),
						new Circle(this, 25, 0xFF0000FF),
						new Circle(this, 25, 0xFFFFFF00),
						new Circle(this, 25, 0xFFFF00FF),
						new Circle(this, 25, 0xFF00FFFF),
						new Circle(this, 25, 0xFFFF0000),
						new Circle(this, 25, 0xFF00FF00),
						new Circle(this, 25, 0xFF0000FF),
						new Circle(this, 25, 0xFFFFFF00),
						new Circle(this, 25, 0xFFFF00FF),
						new Circle(this, 25, 0xFF00FFFF),
						new Circle(this, 25, 0xFFFF0000),
						new Circle(this, 25, 0xFF00FF00),
						new Circle(this, 25, 0xFF0000FF),
						new Circle(this, 25, 0xFFFFFF00),
						new Circle(this, 25, 0xFFFF00FF),
						new Circle(this, 25, 0xFF00FFFF)
				};
	}

	public void draw()
	{
		if (RandomInt.randomInt(0, 100) == 50)
		{
			switch (RandomInt.randomInt(0, 6))
			{
				case 0:
					fill(0x80FFFFFF);
					break;
				case 1:
					fill(0x80FF0000);
					break;
				case 2:
					fill(0x8000FF00);
					break;
				case 3:
					fill(0x800000FF);
					break;
				case 4:
					fill(0x80FFFF00);
					break;
				case 5:
					fill(0x80FF00FF);
					break;
				case 6:
					fill(0x8000FFFF);
					break;
			}
		} else fill(0xA000000);

		rect(0, 0, width, height);

		for (int i = 0; i < circles.length; i++)
		{
			circles[i].update();
		}
	}
}
