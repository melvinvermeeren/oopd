/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package Disco.Disco_V1;

import java.util.Random;

/**
 * Created by Melvin on 14-Feb-2014 13:06.
 */
class RandomInt
{
	/**
	 * Returns a pseudo-random number between min and max, inclusive.
	 * The difference between min and max can be at most
	 * <code>Integer.MAX_VALUE - 1</code>.
	 *
	 * @param min Minimum value.
	 * @param max Maximum value.  Must be greater than min.
	 * @return Integer between min and max, inclusive.
	 * @see java.util.Random#nextInt(int)
	 */
	static int randomInt(int min, int max)
	{

		// Usually this can be a field rather than a method variable
		Random rand = new Random();

		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive
		return rand.nextInt((max - min) + 1) + min;
	}
}
