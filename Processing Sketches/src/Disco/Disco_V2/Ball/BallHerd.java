/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package Disco.Disco_V2.Ball;

import java.util.ArrayList;

/**
 * Created on 28-Feb-2014 11:36.
 * TODO: Fix it, it's broken right now.
 *
 * @author Melvin
 */
public final class BallHerd extends Ball
{
	private static final float
			REACTION_DISTANCE_MAX = 50f,
			LERP_FACTOR = 0.001f,
			HERD_VELOCITY_X_MIN = -3f,
			HERD_VELOCITY_X_MAX = 3f,
			HERD_VELOCITY_Y_MIN = -3f,
			HERD_VELOCITY_Y_MAX = 3f;
	private static ArrayList<BallHerd> ballHerds = new ArrayList<>();

	/**
	 * Constructor with all parameters.
	 *
	 * @param color color of the ball
	 * @param size  diameter of the ball in pixels
	 */
	public BallHerd(int color, int size)
	{
		super(color, size);
		ballHerds.add(this);
	}

	/**
	 * Constructor with color parameter.
	 * Generates a random size.
	 *
	 * @param color color of the ball
	 */
	public BallHerd(int color)
	{
		this(color, getRandomSize());
	}

	/**
	 * Default constructor.
	 * Generates a random color and size.
	 */
	public BallHerd()
	{
		this(getRandomColor());
	}

	/**
	 * Updates position.
	 * Only called by method {@link #update()}.
	 */
	@Override
	protected void updatePosition()
	{
		if (!updateNearestBallHerd()) super.updatePosition(); // No other BallHerd nearby, fallback to regular movement.
	}

	/**
	 * Gets the BallHerd object closest to the current BallHerd object.
	 *
	 * @return {@code true} if success or {@code false} if fail
	 */
	private boolean updateNearestBallHerd()
	{
		BallHerd nearestBallHerd = null;
		float nearestDistance = REACTION_DISTANCE_MAX;

		for (BallHerd ballHerd : ballHerds)
		{
			if (!this.equals(ballHerd))
			{
				float distance = location.dist(ballHerd.location);
				if (distance < 0) distance = -distance;

				if (distance < nearestDistance)
				{
					nearestBallHerd = ballHerd;
					nearestDistance = distance;
				}
			}
		}

		if (nearestBallHerd == null) return false;

		velocity.lerp(nearestBallHerd.location, LERP_FACTOR);

		/* Cap the velocity if it's over the limits. */
		if (velocity.x < HERD_VELOCITY_X_MIN) velocity.x = HERD_VELOCITY_X_MIN;
		else if (velocity.x > HERD_VELOCITY_X_MAX) velocity.x = HERD_VELOCITY_X_MAX;
		if (velocity.y < HERD_VELOCITY_Y_MIN) velocity.y = HERD_VELOCITY_Y_MIN;
		else if (velocity.y > HERD_VELOCITY_Y_MAX) velocity.y = HERD_VELOCITY_Y_MAX;

		location.add(velocity);

		return true;
	}
}
