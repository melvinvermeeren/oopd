/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package Disco.Disco_V2.Ball;

import processing.core.PApplet;
import processing.core.PVector;

import java.util.ArrayList;
import java.util.Random;

import static java.lang.Math.round;

/**
 * Created on 18-Feb-2014 14:26.
 *
 * @author Melvin Vermeeren
 */
public class Ball
{
	protected static final int // Random size limits.
			RANDOM_SIZE_MIN = 10,
			RANDOM_SIZE_MAX = 50;
	protected static final float COLLISION_WINDOW_FACTOR = 0.5f; // Multiply velocity by this value upon window collision.
	protected static final float // Velocity limits.
			VELOCITY_X_MIN = -15f,
			VELOCITY_X_MAX = 15f,
			VELOCITY_Y_MIN = -10f,
			VELOCITY_Y_MAX = 10f;
	protected static final float // Acceleration limits.
			ACCELERATION_X_DIFF = 0.6f, // 6f would be max -3f and max +2.999f.
			ACCELERATION_Y_DIFF = 0.4f;
	protected static ArrayList<Ball> allBalls = new ArrayList<>();
	protected static PApplet parent;
	protected static Random random;
	protected PVector location;
	protected PVector velocity;
	protected int size;
	protected int color;

	/**
	 * Constructor with all parameters.
	 *
	 * @param color color of the ball
	 * @param size  diameter of the ball in pixels
	 */
	public Ball(int color, int size)
	{
		assert (random != null);
		if (color == 0 || color == 0xFF000000) color = getRandomColor(); // Don't allow black color or just 0.

		allBalls.add(this);
		location = new PVector(random.nextInt(parent.width - size), random.nextInt(parent.height - size));
		velocity = new PVector(getRandomVelocity(VELOCITY_X_MIN / 2f, VELOCITY_X_MAX / 2f), getRandomVelocity(VELOCITY_Y_MIN / 2f, VELOCITY_Y_MAX / 2f));
		this.color = color;
		this.size = size;
	}

	/**
	 * Constructor with color parameter.
	 * Generates a random size.
	 *
	 * @param color color of the ball
	 */
	public Ball(int color)
	{
		this(color, getRandomSize());
	}

	/**
	 * Default constructor.
	 * Generates a random color and size.
	 */
	public Ball()
	{
		this(getRandomColor());
	}

	/**
	 * Configure the static variables, must be done prior to creating an object.
	 *
	 * @param parent PApplet canvas
	 */
	public static void setup(PApplet parent)
	{
		assert (Ball.parent == null && Ball.random == null) : "Method called twice.";

		Ball.parent = parent;
		Ball.random = new Random();
	}

	/**
	 * Generates a random acceleration value.
	 * Only called by method {@link #updatePosition()}.
	 *
	 * @param diff total difference from 0 to both positive and negative, "30" would mean "-15" through "15".
	 * @return random acceleration
	 */
	protected static float getRandomAcceleration(float diff)
	{
		assert (random != null);

		diff *= 1000f; // Multiply by 1000 to "fake" precision.
		return (random.nextInt(round(diff) + 1) / 1000f) - (diff / 2000f); // diff / 1000 / 2 works too, this is cleaner.
	}

	/**
	 * Generates a random velocity value.
	 * Only called by constructor {@link #Ball(int, int)}.
	 *
	 * @param min minimum velocity
	 * @param max maximum velocity
	 * @return random velocity
	 */
	protected static float getRandomVelocity(float min, float max)
	{
		assert (random != null);

		/* Multiply by 1000 to "fake" precision. */
		min *= 1000f;
		max *= 1000f;

		int roundedMaxMin = round(max - min);

		/* Check if "max - min" would mess up integer rounding up, if so compensate by incrementing processedMin. */
		int processedMin = (int) min;
		if (roundedMaxMin != (int) (max - min)) processedMin++;

		return (random.nextInt(roundedMaxMin + 1) + processedMin) / 1000f;
	}

	/**
	 * Generates a random size.
	 * Only called by constructor {@link #Ball(int)}.
	 *
	 * @return random size
	 */
	protected static int getRandomSize()
	{
		assert (random != null);
		return random.nextInt(RANDOM_SIZE_MAX - RANDOM_SIZE_MIN + 1) + RANDOM_SIZE_MIN;
	}

	/**
	 * Generates a random color.
	 *
	 * @return random color
	 */
	protected static int getRandomColor()
	{
		assert (random != null);

		switch (random.nextInt(6)) // 6 options, so generate 0 through 5.
		{
			case 0:
				return 0xFFFF0000;
			case 1:
				return 0xFFFFFF00;
			case 2:
				return 0xFF00FF00;
			case 3:
				return 0xFF00FFFF;
			case 4:
				return 0xFF0000FF;
			default:
				return 0xFFFF00FF;
		}
	}

	/**
	 * Updates position and checks for window collision.
	 * TODO: Optimize ball collision.
	 */
	public void update()
	{
		updatePosition();
		checkWindowCollision();

		for (Ball ball : allBalls)
		{
			if (!this.equals(ball)) checkBallCollision(ball);
		}
	}

	/**
	 * Updates position.
	 * Only called by method {@link #update()}.
	 */
	protected void updatePosition()
	{
		velocity.add(getRandomAcceleration(ACCELERATION_X_DIFF), getRandomAcceleration(ACCELERATION_Y_DIFF), 0f);

		/* Cap the velocity if it's over the limits. */
		if (velocity.x < VELOCITY_X_MIN) velocity.x = VELOCITY_X_MIN;
		else if (velocity.x > VELOCITY_X_MAX) velocity.x = VELOCITY_X_MAX;
		if (velocity.y < VELOCITY_Y_MIN) velocity.y = VELOCITY_Y_MIN;
		else if (velocity.y > VELOCITY_Y_MAX) velocity.y = VELOCITY_Y_MAX;

		location.add(velocity); // Update the location of the ball.
	}

	/**
	 * Checks for window collision.
	 * Only called by method {@link #update()}.
	 */
	protected void checkWindowCollision()
	{
		/* Check for window collision. */
		if (location.x <= 0) windowCollisionUpdateX(true);
		else if (location.x >= parent.width - size) windowCollisionUpdateX(false);
		if (location.y <= 0) windowCollisionUpdateY(true);
		else if (location.y >= parent.height - size) windowCollisionUpdateY(false);
	}

	/**
	 * Updates location and velocity to "bounce off" window edges. For x-axis only.
	 * Only called by method {@link #checkWindowCollision()}.
	 *
	 * @param atZero {@code true} if {@code x <= 0}, else {@code false}
	 */
	protected void windowCollisionUpdateX(boolean atZero)
	{
		if (atZero) location.x = 0;
		else location.x = parent.width - size;
		velocity.x *= -COLLISION_WINDOW_FACTOR;
	}

	/**
	 * Updates location and velocity to "bounce off" window edges. For y-axis only.
	 * Only called by method {@link #checkWindowCollision()}.
	 *
	 * @param atZero {@code true} if {@code y <= 0}, else {@code false}
	 */
	protected void windowCollisionUpdateY(boolean atZero)
	{
		if (atZero) location.y = 0;
		else location.y = parent.height - size;
		velocity.y *= -COLLISION_WINDOW_FACTOR;
	}

	protected void checkBallCollision(Ball other)
	{
		float size = this.size / 2;
		float other_size = other.size / 2;

		float m = size * .1f;
		float other_m = other_size * .1f;

		// get distances between the balls components
		PVector bVect = PVector.sub(other.location, location);

		// calculate magnitude of the vector separating the balls
		float bVectMag = bVect.mag();

		if (bVectMag < size + other_size)
		{
			// get angle of bVect
			double theta = bVect.heading();
			// precalculate trig values
			float sine = (float) Math.sin(theta);
			float cosine = (float) Math.cos(theta);

			/* bTemp will hold rotated ball locations. You just need to worry about bTemp[1] location*/
			PVector[] bTemp = {
					new PVector(), new PVector()
			};

			/* this ball's location is relative to the other
			   so you can use the vector between them (bVect) as the
			   reference point in the rotation expressions.
			   bTemp[0].location.x and bTemp[0].location.y will initialize
			   automatically to 0.0, which is what you want
			   since b[1] will rotate around b[0] */
			bTemp[1].x = cosine * bVect.x + sine * bVect.y;
			bTemp[1].y = cosine * bVect.y - sine * bVect.x;

			// rotate Temporary velocities
			PVector[] vTemp = {
					new PVector(), new PVector()
			};

			vTemp[0].x = cosine * velocity.x + sine * velocity.y;
			vTemp[0].y = cosine * velocity.y - sine * velocity.x;
			vTemp[1].x = cosine * other.velocity.x + sine * other.velocity.y;
			vTemp[1].y = cosine * other.velocity.y - sine * other.velocity.x;

			/* Now that velocities are rotated, you can use 1D
			   conservation of momentum equations to calculate
			   the final velocity along the x-axis. */
			PVector[] vFinal = {
					new PVector(), new PVector()
			};

			// final rotated velocity for b[0]
			vFinal[0].x = ((m - other_m) * vTemp[0].x + 2 * other_m * vTemp[1].x) / (m + other_m);
			vFinal[0].y = vTemp[0].y;

			// final rotated velocity for b[0]
			vFinal[1].x = ((other_m - m) * vTemp[1].x + 2 * m * vTemp[0].x) / (m + other_m);
			vFinal[1].y = vTemp[1].y;

			// hack to avoid clumping
			bTemp[0].x += vFinal[0].x;
			bTemp[1].x += vFinal[1].x;

			/* Rotate ball locations and velocities back
			   Reverse signs in trig expressions to rotate
			   in the opposite direction */
			// rotate balls
			PVector[] bFinal = {
					new PVector(), new PVector()
			};

			bFinal[0].x = cosine * bTemp[0].x - sine * bTemp[0].y;
			bFinal[0].y = cosine * bTemp[0].y + sine * bTemp[0].x;
			bFinal[1].x = cosine * bTemp[1].x - sine * bTemp[1].y;
			bFinal[1].y = cosine * bTemp[1].y + sine * bTemp[1].x;

			// update balls to screen location
			other.location.x = location.x + bFinal[1].x;
			other.location.y = location.y + bFinal[1].y;

			location.add(bFinal[0]);

			// update velocities
			velocity.x = cosine * vFinal[0].x - sine * vFinal[0].y;
			velocity.y = cosine * vFinal[0].y + sine * vFinal[0].x;
			other.velocity.x = cosine * vFinal[1].x - sine * vFinal[1].y;
			other.velocity.y = cosine * vFinal[1].y + sine * vFinal[1].x;
		}
	}

	/**
	 * Draws the object on the parent PApplet canvas.
	 */
	public void draw()
	{
		assert (parent != null);

		parent.fill(color);
		parent.ellipse(location.x, location.y, size, size);
	}
}
