/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package Disco.Disco_V2.Ball;

/**
 * Created on 25-Feb-2014 16:44.
 *
 * @author Melvin Vermeeren
 */
public final class BallDisco extends Ball
{
	/**
	 * Extends {@link Ball#update()}, randomizes color every frame.
	 */
	@Override
	public void update()
	{
		super.update();
		randomizeColor();
	}

	/**
	 * Randomizes the color.
	 * Only called by method {@link #update()}.
	 */
	private void randomizeColor()
	{
		int colorCopy = color;

		do color = getRandomColor();
		while (color == colorCopy);
	}
}
