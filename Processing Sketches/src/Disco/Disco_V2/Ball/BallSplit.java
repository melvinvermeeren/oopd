/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package Disco.Disco_V2.Ball;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created on 28-Feb-2014 13:19.
 *
 * @author Melvin
 */
public final class BallSplit extends Ball
{
	private static final int BALLSPLIT_REMOVE_WHEN_SIZE = 5; // When smaller or equal to.
	private static final float BALLSPlIT_SPLIT_FACTOR = 0.75f;
	private ArrayList<BallSplit> childBalls;

	/**
	 * Constructor with all parameters.
	 *
	 * @param color color of the ball
	 * @param size  diameter of the ball in pixels
	 */
	public BallSplit(int color, int size)
	{
		super(color, size);
		childBalls = new ArrayList<>();
	}

	/**
	 * Constructor with color parameter.
	 *
	 * @param color color of the ball
	 */
	public BallSplit(int color)
	{
		this(color, getRandomSize());
	}

	/**
	 * Default constructor.
	 * Generates a random color.
	 */
	public BallSplit()
	{
		this(getRandomColor());
	}

	/**
	 * Remove a ball object from the internal ArrayList containing all balls.
	 *
	 * @param ball ball object to remove
	 */
	private void removeBall(Ball ball)
	{
		allBalls.remove(ball);
	}

	/**
	 * Updates location and velocity to "bounce off" window edges. For x-axis only.
	 * Only called by method {@link #checkWindowCollision()}.
	 *
	 * @param atZero {@code true} if {@code x <= 0}, else {@code false}
	 */
	@Override
	protected void windowCollisionUpdateX(boolean atZero)
	{
		super.windowCollisionUpdateX(atZero);
		size *= BALLSPlIT_SPLIT_FACTOR;
		childBalls.add(new BallSplit(getRandomColor(), size));
	}

	/**
	 * Updates location and velocity to "bounce off" window edges. For y-axis only.
	 * Only called by method {@link #checkWindowCollision()}.
	 *
	 * @param atZero {@code true} if {@code y <= 0}, else {@code false}
	 */
	@Override
	protected void windowCollisionUpdateY(boolean atZero)
	{
		super.windowCollisionUpdateY(atZero);
		size *= BALLSPlIT_SPLIT_FACTOR;
		childBalls.add(new BallSplit(getRandomColor(), size));
	}

	/**
	 * Updates position and checks for window collision.
	 */
	@Override
	public void update()
	{
		super.update();

		Iterator iterator = childBalls.iterator();
		while (iterator.hasNext())
		{
			BallSplit ballSplit = (BallSplit) iterator.next();

			if (ballSplit.size <= BALLSPLIT_REMOVE_WHEN_SIZE)
			{
				removeBall(ballSplit);
				iterator.remove();
			}
			else ballSplit.update();
		}
	}

	/**
	 * Draws the object on the parent PApplet canvas.
	 */
	@Override
	public void draw()
	{
		super.draw();

		for (BallSplit ballSplit : childBalls)
		{
			ballSplit.draw();
		}
	}
}
