/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package Disco.Disco_V2;

import Disco.Disco_V2.Ball.Ball;
import Disco.Disco_V2.Ball.BallDisco;
import Disco.Disco_V2.Ball.BallHerd;
import Disco.Disco_V2.Ball.BallSplit;
import processing.core.PApplet;

/**
 * Created on 18-Feb-2014 13:40.
 *
 * @author Melvin Vermeeren
 */
public class Disco_V2 extends PApplet
{
	private static final int BALLSPLIT_SIZE = 100;
	private static final int
			NUMBER_OF_BALLS = 0,
			NUMBER_OF_BALLDISCOS = 0,
			NUMBER_OF_BAllHERDS = 0,
			NUMBER_OF_BALLSPLITS = 1;

	public static Ball[] balls;

	public static void main(String[] args)
	{
		/* Configure Processing Applet. */
		PApplet.main(new String[]{"--present", "Disco.Disco_V2.Disco_V2"});
	}

	public void setup()
	{
		/* Setup drawing components. */
		noStroke();
		ellipseMode(CORNER);

		/* Setup canvas. */
		frameRate(60f);
		size(displayWidth, displayHeight);
		delay(10); // Let new size initialize properly before drawing the initial background and generating objects.
		background(0xFF000000); // Prevent fading into black from white in the first few frames.

		/* Setup Ball class. */
		Ball.setup(this);
		balls = new Ball[NUMBER_OF_BALLS + NUMBER_OF_BALLDISCOS + NUMBER_OF_BAllHERDS + NUMBER_OF_BALLSPLITS];

		/* Add Ball objects. */
		for (int i = 0;
		     i < NUMBER_OF_BALLS;
		     i++)
		{
			balls[i] = new Ball();
		}

		/* Add BallDisco objects. */
		for (int i = NUMBER_OF_BALLS;
		     i < NUMBER_OF_BALLS + NUMBER_OF_BALLDISCOS;
		     i++)
		{
			balls[i] = new BallDisco();
		}

		/* Add BallHerd objects. */
		for (int i = NUMBER_OF_BALLS + NUMBER_OF_BALLDISCOS;
		     i < NUMBER_OF_BALLS + NUMBER_OF_BALLDISCOS + NUMBER_OF_BAllHERDS;
		     i++)
		{
			balls[i] = new BallHerd();
		}

		/*Add BallSplit objects. */
		for (int i = NUMBER_OF_BALLS + NUMBER_OF_BALLDISCOS + NUMBER_OF_BAllHERDS;
		     i < NUMBER_OF_BALLS + NUMBER_OF_BALLDISCOS + NUMBER_OF_BAllHERDS + NUMBER_OF_BALLSPLITS;
		     i++)
		{
			balls[i] = new BallSplit(0, BALLSPLIT_SIZE);
		}
	}

	public void draw()
	{
		/* Draw background. */
		fill(0x30000000);
		rect(0, 0, width, height);

		/* Update and draw the Ball objects. */
		for (Ball ball : balls)
		{
			ball.update();
			ball.draw();
		}
	}
}
