/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package Figuren;

import processing.core.PApplet;

/**
 * Created by Melvin on 24-Feb-2014 11:40.
 */
class Rechthoek extends FigureCore
{
	private float breedte, hoogte;

	public Rechthoek(float x, float y, float breedte, float hoogte)
	{
		super(x, y);
		this.breedte = breedte;
		this.hoogte = hoogte;
	}

	public void tekenRechthoek(PApplet p)
	{
		p.noStroke();
		p.fill(kleur);
		p.rect(x, y, breedte, hoogte);
	}
}
