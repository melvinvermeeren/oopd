/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package Figuren;

import processing.core.PApplet;

/**
 * Created by Melvin on 24-Feb-2014 11:39.
 */
class Cirkel extends FigureCore
{
	private float diameter;

	public Cirkel(float x, float y, float diameter)
	{
		super(x, y);
		this.diameter = diameter;
	}

	public void tekenCirkel(PApplet p)
	{
		p.noStroke();
		p.fill(kleur);
		p.ellipse(x, y, diameter, diameter);
	}
}
