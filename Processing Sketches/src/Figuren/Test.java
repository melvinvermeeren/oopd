/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package Figuren;

import processing.core.PApplet;

/**
 * Created by Melvin on 24-Feb-2014 11:46.
 */
public class Test extends PApplet
{
	public static void main(String[] args)
	{
		PApplet.main("Figuren.Test");
	}

	Rechthoek r = new Rechthoek(10, 10, 50, 60);
	Cirkel c = new Cirkel(90, 80, 30);

	public void setup()
	{
		size(400, 400);
		background(0);
		r.setSnelheid(1, 1);
		c.setSnelheid(1, 0);
	}

	public void draw()
	{
		background(0);
		r.doeStap();
		c.doeStap();
		r.tekenRechthoek(this);
		c.tekenCirkel(this);
	}
}
