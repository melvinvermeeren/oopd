/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package TicketMachine;

/**
 * Created by Melvin on 11-Feb-2014 15:51.
 */
class Core
{
	public static void main(String[] args)
	{
		TicketMachine[] ticketMachines =
				{
						new TicketMachine("James Bond", 2950),
						new TicketMachine("G de G", 1995),
						new TicketMachine("Sneeuwwitje", 995)
				};

		for (TicketMachine ticketMachine : ticketMachines)
		{
			System.out.println(ticketMachine);
			System.out.println();
		}

	}
}
