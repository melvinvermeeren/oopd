/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package TicketMachine;

/**
 * Created by Melvin on 11-Feb-2014 15:54.
 */
class TicketMachine
{
	private String film;
	private int price;
	private int currentAmount = 0;
	private int totalAmount = 0;

	TicketMachine(String film, int price)
	{
		this.film = film;
		this.price = price;
	}

	private boolean paid()
	{
		return currentAmount >= price;
	}

	private void insertAmount(int amount)
	{
		this.currentAmount += amount;
	}

	public String toString()
	{
		return "film: " + this.film +
				"\nprice: " + this.price +
				"\ncurrentAmount: " + this.currentAmount +
				"\ntotalAmount: " + this.totalAmount;
	}
}
