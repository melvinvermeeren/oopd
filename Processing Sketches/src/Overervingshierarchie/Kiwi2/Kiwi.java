/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package Overervingshierarchie.Kiwi2;

import Overervingshierarchie.Dier;

/**
 * Created by Melvin on 23-Feb-2014 23:15.
 */
class Kiwi extends Dier
{
	private int loopSnelheid;

	public Kiwi(String naam, int loopSnelheid)
	{
		super(naam);
		this.loopSnelheid = loopSnelheid;
	}
}
