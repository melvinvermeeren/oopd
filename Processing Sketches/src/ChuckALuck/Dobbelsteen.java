/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package ChuckALuck;

import java.util.Random;

class Dobbelsteen
{
	private int maxOgen;
	private int aantalOgen;

	public Dobbelsteen(int aantalVlakken)
	{
		this.maxOgen = aantalVlakken + 1;
	}

	public int werp()
	{
		Random dice = new Random();
		aantalOgen = dice.nextInt(maxOgen - 1);
		aantalOgen = aantalOgen + 1;
		return aantalOgen;
	}

	public int getAantalOgen()
	{
		return aantalOgen;
	}

	public String toString()
	{
		return "" + aantalOgen + "";
	}
}