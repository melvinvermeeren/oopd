/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package ChuckALuck;

/**
 * Created by Melvin on 17-Feb-2014 10:09.
 */
class MainApp
{
	public static void main(String[] args)
	{
		ChuckALuckSpel spel = new ChuckALuckSpel(250);

		while (true)
		{
			spel.speelRonde(2, 10);
			System.out.println(spel);
			if (spel.getSaldo() <= 0)
			{
				System.out.println("Saldo 0 of negatief, game over!");
				break;
			}

			try
			{
				Thread.sleep(250);
			} catch (InterruptedException ie)
			{
				System.out.println("InterruptedException caught in MainApp.java.");
			}
		}
	}
}
