/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package ChuckALuck;

class ChuckALuckSpel
{
	private int saldo;
	private int ronde;
	private int geluksgetal;
	private Dobbelbeker beker;

	public ChuckALuckSpel(int saldo)
	{
		this.saldo = saldo;
		this.ronde = 0;
		this.beker = new Dobbelbeker();
	}

	public int getSaldo()
	{
		return saldo;
	}

	public void speelRonde(int geluksgetal, int inzet)
	{
		this.geluksgetal = geluksgetal;
		ronde++;
		beker.werp();
		int overeenkomst = beker.getAantalOvereenkomsten(geluksgetal);
		switch (overeenkomst)
		{
			case 0:
				saldo = saldo - inzet;
				break;
			case 1:
				break;
			case 2:
				saldo += (inzet);
				break;
			case 3:
				saldo += (inzet * 9);
				System.out.println("LUUUCKY JE BENT RIJK GELD GELD GELD GELD!");
				try
				{
					Thread.sleep(1000);
				} catch (InterruptedException ie)
				{
					System.out.println("InterruptedException caught in ChuckALuckSpel.java.");
				}
				break;
		}
	}

	public String toString()
	{
		int waardes[] = beker.getWaardes();
		String s = "";
		s += "Ronde \t\t:\t" + ronde + "\n" +
				"Geluksgetal \t:\t" + geluksgetal +
				"\nWorp \t\t:\t";
		for (int i = 0; i < waardes.length; i++)
		{
			s += waardes[i] + " ";
		}
		s += "\nSaldo \t\t:\t" + saldo + "\n";
		return s;
	}
}
