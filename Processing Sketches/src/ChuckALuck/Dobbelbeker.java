/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package ChuckALuck;

/**
 * Created by Melvin on 17-Feb-2014 10:09.
 */
class Dobbelbeker
{
	private Dobbelsteen[] dobbelstenen;

	Dobbelbeker()
	{
		dobbelstenen = new Dobbelsteen[]
				{
						new Dobbelsteen(6),
						new Dobbelsteen(6),
						new Dobbelsteen(6)
				};
	}

	void werp()
	{
		for (Dobbelsteen dobbelsteen : dobbelstenen)
		{
			dobbelsteen.werp();
		}
	}

	int[] getWaardes()
	{
		int[] waardes = new int[dobbelstenen.length];

		for (int i = 0; i < waardes.length; i++)
		{
			waardes[i] = dobbelstenen[i].getAantalOgen();
		}

		return waardes;
	}

	int getAantalOvereenkomsten(int gekozenGetal)
	{
		int aantalOvereenkomsten = 0;
		int[] waardes = getWaardes();

		for (int i = 0; i < waardes.length; i++)
		{
			if (waardes[i] == gekozenGetal) aantalOvereenkomsten++;
		}

		return aantalOvereenkomsten;
	}

	public String toString()
	{
		String string = "worp:";

		for (Dobbelsteen dobbelsteen : dobbelstenen)
		{
			string += " " + dobbelsteen.toString();
		}

		return string;
	}
}
