/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package ChuckALuck;

import java.util.Arrays;

/**
 * Created by Melvin on 17-Feb-2014 11:17.
 */
class TestDobbelbeker
{
	final static private boolean printExtraInfo = false;

	public static void main(String[] args)
	{
		Dobbelbeker beker = new Dobbelbeker();

		beker.werp();

		if (printExtraInfo)
		{
			System.out.println("Waardes:");
			System.out.println(Arrays.toString(beker.getWaardes()));
			System.out.println("Overeenkomsten: " + beker.getAantalOvereenkomsten(3));
		}
		System.out.println(beker);

		beker.werp();

		if (printExtraInfo)
		{
			System.out.println("Waardes:");
			System.out.println(Arrays.toString(beker.getWaardes()));
			System.out.println("Overeenkomsten: " + beker.getAantalOvereenkomsten(2));
		}
		System.out.println(beker);
	}
}
