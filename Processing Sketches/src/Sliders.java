/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

import processing.core.PApplet;

/**
 * Created by Melvin on 09-Feb-2014 17:25.
 */
public class Sliders extends PApplet
{
	Slider slider;

	public void setup()
	{
		size(300, 150);
		slider = new Slider(50, 200, 50, 5);
		println(slider);
	}

	public void draw()
	{
		background(0);
		slider.draw();
	}

	/**
	 * Slider object.
	 */
	private class Slider
	{
		private float x;
		private float y;
		private float sliderWidth;
		private float sliderHeight;
		private int positions;

		/**
		 * Creates a new Slider object.
		 * x-coordinate is calculated automatically.
		 *
		 * @param y            y-coordinate
		 * @param sliderWidth  width
		 * @param sliderHeight height
		 * @param positions    number of positions
		 */
		private Slider(float y, float sliderWidth, float sliderHeight, int positions)
		{
			this.y = y;
			this.sliderWidth = sliderWidth;
			this.sliderHeight = sliderHeight;
			this.positions = positions;

			this.x = (width - this.sliderWidth) / 2;
		}

		/**
		 * Draws the slider object on the screen.
		 */
		private void draw()
		{
			/* Background of the slider. */
			noStroke();
			fill(255);
			rect(this.x, this.y, this.sliderWidth, this.sliderHeight);

			/* The actual slider itself. */
			float blockWidth = this.sliderWidth / this.positions;
			fill(0xFF2257F0);
			rect(this.x + this.determinePosition() * blockWidth, this.y, blockWidth, this.sliderHeight);
		}

		/**
		 * Determines the position of the slider.
		 *
		 * @return position of the slider.
		 */
		private int determinePosition()
		{
			float blockWidth = this.sliderWidth / this.positions;

			if (mouseX < this.x) return 0;
			else if (mouseX >= this.sliderWidth + this.x) return this.positions - 1;
			else return floor((mouseX - this.x) / blockWidth);
		}

		/**
		 * This is printed when a Slider object is being printed.
		 *
		 * @return output String
		 */
		public String toString()
		{
			return "x: " + Float.toString(this.x) +
					"\ny: " + Float.toString(this.y) +
					"\nsliderWidth: " + Float.toString(this.sliderWidth) +
					"\nsliderHeight: " + Float.toString(this.sliderHeight) +
					"\npositions: " + Integer.toString(this.positions);
		}
	}
}
