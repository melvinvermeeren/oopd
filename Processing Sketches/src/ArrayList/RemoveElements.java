/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package ArrayList;

import java.util.ArrayList;

/**
 * Created by Melvin on 17-Feb-2014 21:33.
 */
class RemoveElements
{
	public static void main(String[] args)
	{
		ArrayList<String> list = new ArrayList<String>();

		for (int i = 0; i < 4; i++)
		{
			list.add("element: " + i);
		}

		for (String element : list)
		{
			System.out.println(element);
		}

		System.out.println("-------");

		for (int i = list.size() - 1; i >= 0; i--)
		{
			list.remove(i);
			/* Removing using a for-each loop isn't allowed, it throws an exception. */
		}

		for (String element : list)
		{
			System.out.println(element);
		}
	}
}
