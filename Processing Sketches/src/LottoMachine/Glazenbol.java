/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package LottoMachine;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Melvin on 18-Feb-2014 16:22.
 */
class Glazenbol
{
	private ArrayList<Lottobal> lottoballs;

	public Glazenbol(int numberOfBalls)
	{
		lottoballs = new ArrayList<Lottobal>();

		for (int i = 0; i < numberOfBalls; i++) lottoballs.add(i, new Lottobal(i + 1));
	}

	public void addBall(Lottobal lottoball)
	{
		lottoballs.add(lottoball);
	}

	public Lottobal scoopBall()
	{
		Random random = new Random();
		int randomNumber = random.nextInt(lottoballs.size());

		Lottobal lottobal = lottoballs.get(randomNumber);
		lottoballs.remove(randomNumber);

		return lottobal;
	}

	public String toString()
	{
		String numbers = "";

		for (Lottobal element : lottoballs)
		{
			numbers += " ";
			numbers += element.getNumber();
		}

		return "Glazenbol{" +
				"lottoballs =" + numbers +
				'}';
	}
}
