/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package LottoMachine;

/**
 * Created by Melvin on 18-Feb-2014 16:01.
 */
class Lottobal
{
	private int number;

	public Lottobal(int number)
	{
		this.number = number;
	}

	public boolean isNumberBiggerThan(Lottobal lottobal)
	{
		return this.number > lottobal.number;
	}

	public int getNumber()
	{
		return number;
	}

	public String toString()
	{
		return "Lottobal{" +
				"number=" + number +
				'}';
	}
}
