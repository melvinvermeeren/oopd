/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package LottoMachine;

import java.util.ArrayList;

/**
 * Created by Melvin on 18-Feb-2014 16:34.
 */
class Scorebord
{
	private ArrayList<Lottobal> lottoballs;
	private Lottobal bonusBall;

	public Scorebord()
	{
		lottoballs = new ArrayList<Lottobal>();
	}

	public Lottobal[] getLottoballs()
	{
		Lottobal[] lottoballs = new Lottobal[this.lottoballs.size()];

		for (int i = 0; i < this.lottoballs.size(); i++)
		{
			lottoballs[i] = this.lottoballs.get(i);
		}

		return lottoballs;
	}

	public Lottobal getBonusBall()
	{
		return bonusBall;
	}

	public void clear()
	{
		lottoballs.clear();
		bonusBall = null;
	}

	public void addBall(Lottobal lottobal)
	{
		lottoballs.add(lottobal);
	}

	public void addBonusBall(Lottobal lottobal)
	{
		bonusBall = lottobal;
	}

	public void sortBalls()
	{
		Lottobal tempBall;

		for (int i = lottoballs.size() - 2; i >= 0; i--)
		{
			tempBall = lottoballs.get(i);

			for (int j = lottoballs.size() - 1; j > i; j--)
			{
				if (tempBall.isNumberBiggerThan(lottoballs.get(j)))
				{
					moveBall(i, j);
					break;
				}
			}
		}
	}

	private void moveBall(int fromIndex, int toIndex)
	{
		Lottobal tempBall = lottoballs.get(fromIndex);

		for (int i = fromIndex; i < toIndex; i++)
		{
			lottoballs.set(i, lottoballs.get(i + 1));
		}

		lottoballs.set(toIndex, tempBall);
	}

	public void print()
	{
		System.out.print("Ballen:");

		for (Lottobal lottobal : lottoballs)
		{
			System.out.print(" ");
			System.out.print(lottobal.getNumber());
		}

		System.out.print(" Bonus: ");
		System.out.println(bonusBall.getNumber());
	}

	public String toString()
	{
		String numbers = "";

		for (Lottobal element : lottoballs)
		{
			numbers += " ";
			numbers += element.getNumber();
		}

		return "Scorebord{" +
				"lottoballs =" + numbers +
				", bonusBall = " + bonusBall +
				'}';
	}
}
