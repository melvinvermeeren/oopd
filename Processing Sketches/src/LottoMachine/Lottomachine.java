/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package LottoMachine;

/**
 * Created by Melvin on 18-Feb-2014 16:45.
 */
class Lottomachine
{
	private Glazenbol glazenbol;
	private Scorebord scorebord;

	public Lottomachine()
	{
		glazenbol = new Glazenbol(45);
		scorebord = new Scorebord();
	}

	public void drawLotto()
	{
		/* Fill scorebord with random balls from glazenbol. */
		for (int i = 0; i < 6; i++)
		{
			scorebord.addBall(glazenbol.scoopBall());
		}
		scorebord.addBonusBall(glazenbol.scoopBall());

		/* Sort the balls in scorebord and print the results. */
		scorebord.sortBalls();
		scorebord.print();

		/* Collect all the balls from scorebord and put them back in glazenbol. */
		collectBalls();
	}

	private void collectBalls()
	{
		Lottobal[] lottoballs = scorebord.getLottoballs();
		for (Lottobal element : lottoballs) glazenbol.addBall(element);

		glazenbol.addBall(scorebord.getBonusBall());

		scorebord.clear();
	}
}
