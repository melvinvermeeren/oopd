/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package Damstenen;

import processing.core.PApplet;

/**
 * Created by Melvin on 10-Feb-2014 22:52.
 * <p/>
 * Dambord object, contains Damsteen objects.
 */
class Dambord
{
	private PApplet parent;
	Damsteen[] damstenen;

	/**
	 * Creates a new Dambord object.
	 *
	 * @param parent PApplet parent
	 */
	Dambord(PApplet parent)
	{
		this.parent = parent;

		damstenen = new Damsteen[]
				{
						new Damsteen(this.parent, 100, 100, 0xFF000000, 25, 0xFFFFFFFF),
						new Damsteen(this.parent, 200, 100, 0xFF000000, 25, 0xFFFFFFFF),
						new Damsteen(this.parent, 100, 200, 0xFFFFFFFF, 25, 0xFF000000),
						new Damsteen(this.parent, 200, 200, 0xFFFFFFFF, 25, 0xFF000000)
				};
	}

	/**
	 * Draw the Dambord object on the screen.
	 */
	void draw()
	{
		for (int i = 0; i < damstenen.length; i++) damstenen[i].draw();
	}
}
