/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package Damstenen;

import processing.core.PApplet;

/**
 * Created by Melvin on 2/7/14.
 */
public class Damstenen extends PApplet
{
	Dambord dambord = new Dambord(this);

	/**
	 * Setup runs once on startup.
	 */
	public void setup()
	{
		size(300, 300);
		noLoop();

		strokeWeight(3);
		ellipseMode(CENTER);

		dambord.damstenen[0].isSelected = true;
	}

	/**
	 * Draw keeps looping after setup.
	 */
	public void draw()
	{
		background(100, 0, 0);
		dambord.draw();
	}
}
