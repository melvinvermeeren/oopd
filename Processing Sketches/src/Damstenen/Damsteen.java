/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package Damstenen;

import processing.core.PApplet;

/**
 * Created by Melvin on 10-Feb-2014 22:44
 * <p/>
 * Damsteen object.
 * Contains x-coordinate, y-coordinate, main colour, diameter and outline colour.
 */
class Damsteen
{
	private PApplet parent;
	private int x; // x-coordinate.
	private int y; // y-coordinate.
	private int color;  // Main colour.
	private int diameter; // Diameter in pixels.
	private int outline; // Outline color.
	boolean isSelected = false; // If Damsteen is selected put a stroke around it.

	/**
	 * Create a new Damsteen object.
	 *
	 * @param parent   PApplet parent
	 * @param x        x-coordinate
	 * @param y        y-coordinate
	 * @param color    main colour
	 * @param diameter diameter
	 * @param outline  outline colour
	 */
	Damsteen(PApplet parent, int x, int y, int color, int diameter, int outline)
	{
		this.parent = parent;
		this.x = x;
		this.y = y;
		this.color = color;
		this.diameter = diameter;
		this.outline = outline;
	}

	/**
	 * Create a new Damsteen object.
	 *
	 * @param parent   PApplet parent
	 * @param x        x-coordinate
	 * @param y        y-coordinate
	 * @param color    main colour
	 * @param diameter diameter
	 */
	Damsteen(PApplet parent, int x, int y, int color, int diameter)
	{
		this(parent, x, y, color, diameter, color);
	}

	/**
	 * Draws the Damsteen object on the screen.
	 */
	void draw()
	{
		parent.fill(this.color);

		if (this.isSelected) parent.stroke(this.outline);
		else parent.noStroke();

		parent.ellipse(this.x, this.y, this.diameter, this.diameter);
	}

	/**
	 * This is printed when a Damsteen object is being printed.
	 *
	 * @return output String
	 */
	public String toString()
	{
		return "x: " + Integer.toString(this.x) +
				"\ny: " + Integer.toString(this.y) +
				"\ncolor: " + Integer.toString(this.color) +
				"\ndiameter: " + Integer.toString(this.diameter) +
				"\noutline: " + Integer.toString(this.outline);
	}
}
