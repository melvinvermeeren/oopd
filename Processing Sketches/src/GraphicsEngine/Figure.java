/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package GraphicsEngine;

/**
 * Created on 10-Mar-2014 23:33.
 *
 * @author Melvin
 */
public abstract class Figure extends DisplayObject
{
	protected int color;

	public int getColor()
	{
		return color;
	}

	public void setColor(int color)
	{
		this.color = color;
	}
}
