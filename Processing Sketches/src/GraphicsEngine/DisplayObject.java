/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package GraphicsEngine;

import processing.core.PApplet;

import java.util.ArrayList;

/**
 * Created on 10-Mar-2014 23:12.
 *
 * @author Melvin
 */
public abstract class DisplayObject
{
	protected float x, y, vx, vy, ax, ay, width, height;
	protected boolean visible;
	protected ArrayList<Responder> responders;

	public DisplayObject()
	{
		responders = new ArrayList<>();
	}

	public void step()
	{}

	public void mousePressed(int mouseX, int mouseY)
	{
		if (visible && isCursorOnObject(mouseX, mouseY))
		{
			for (Responder responder : responders)
			{
				responder.respond();
			}
		}
	}

	public void addResponder(Responder responder)
	{
		responders.add(responder);
	}

	public void removeResponder(Responder responder)
	{
		responders.remove(responder);
	}

	public abstract void draw(PApplet parent, float startX, float startY);

	protected abstract boolean isCursorOnObject(int mouseX, int mouseY);

	public float getX()
	{
		return x;
	}

	public void setX(float x)
	{
		this.x = x;
	}

	public float getY()
	{
		return y;
	}

	public void setY(float y)
	{
		this.y = y;
	}

	public float getVx()
	{
		return vx;
	}

	public void setVx(float vx)
	{
		this.vx = vx;
	}

	public float getVy()
	{
		return vy;
	}

	public void setVy(float vy)
	{
		this.vy = vy;
	}

	public float getAx()
	{
		return ax;
	}

	public void setAx(float ax)
	{
		this.ax = ax;
	}

	public float getAy()
	{
		return ay;
	}

	public void setAy(float ay)
	{
		this.ay = ay;
	}

	public float getWidth()
	{
		return width;
	}

	public void setWidth(float width)
	{
		this.width = width;
	}

	public float getHeight()
	{
		return height;
	}

	public void setHeight(float height)
	{
		this.height = height;
	}

	public boolean isVisible()
	{
		return visible;
	}

	public void setVisible(boolean visible)
	{
		this.visible = visible;
	}
}
