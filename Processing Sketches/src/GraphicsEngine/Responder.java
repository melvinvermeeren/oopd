/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package GraphicsEngine;

/**
 * Created on 10-Mar-2014 23:08.
 *
 * @author Melvin
 */
public interface Responder
{
	void respond();
}
