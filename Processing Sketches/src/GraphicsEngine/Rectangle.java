/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package GraphicsEngine;

import processing.core.PApplet;

/**
 * Created on 10-Mar-2014 23:34.
 *
 * @author Melvin
 */
public class Rectangle extends Figure
{
	@Override
	public void draw(PApplet parent, float startX, float startY)
	{
		if (visible)
		{
			parent.fill(color);
			parent.rect(startX + x, startY + y, width, height);
		}
	}

	@Override
	public boolean isCursorOnObject(int mouseX, int mouseY)
	{
		return (mouseX >= x && mouseX <= x + width &&
				mouseY >= y && mouseY <= y + height);
	}
}
