/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package GraphicsEngine;

/**
 * Created on 10-Mar-2014 23:09.
 *
 * @author Melvin
 */
public class HelloResponder implements Responder
{
	@Override
	public void respond()
	{
		System.out.println("Hello!");
	}
}
