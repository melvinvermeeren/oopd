/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package GraphicsEngine;

import processing.core.PApplet;

/**
 * Created on 10-Mar-2014 23:30.
 *
 * @author Melvin
 */
public class GraphicsEngine extends PApplet
{
	private DisplayObject[] displayObjects;

	public static void main(String[] args)
	{
		PApplet.main(new String[]{"GraphicsEngine.GraphicsEngine"});
	}

	@Override
	public void setup()
	{
		size(150, 150);
		noStroke();

		displayObjects = new DisplayObject[]
				{
						new Rectangle()
				};

		displayObjects[0].setX(50f);
		displayObjects[0].setY(50f);
		displayObjects[0].setWidth(50f);
		displayObjects[0].setHeight(50f);
		displayObjects[0].setVisible(true);
		((Rectangle)displayObjects[0]).setColor(0xFFFF0000);
		displayObjects[0].addResponder(new HelloResponder());
	}

	@Override
	public void draw()
	{
		background(0xFF000000);

		for (DisplayObject displayObject : displayObjects)
		{
			displayObject.draw(this, 0f, 0f);
		}
	}

	@Override
	public void mousePressed()
	{
		for (DisplayObject displayObject : displayObjects)
		{
			displayObject.mousePressed(mouseX, mouseY);
		}
	}
}
