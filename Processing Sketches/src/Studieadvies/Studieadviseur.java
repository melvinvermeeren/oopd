/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package Studieadvies;

/**
 * Created by Melvin on 24-Feb-2014 09:37.
 */
class Studieadviseur
{
	public static boolean krijgtPositiefStudieAdvies(Student s)
	{
		int hoeveelheidVoldoendes = 0;

		for (int cijfer : s.getCijfers())
		{
			if (cijfer >= 6) hoeveelheidVoldoendes++;
		}

		return hoeveelheidVoldoendes >= 4;
	}
}
