/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package Studieadvies;

import java.util.Random;

/**
 * Created by Melvin on 24-Feb-2014 09:42.
 */
class StudieadviseurTest
{
	public static void main(String[] args)
	{
		Random r = new Random();

		Student s1 = new Student("persoon 1");
		for (int i = 0; i < 8; i++)
		{
			s1.setCijfer(i, r.nextInt(10) + 1);
		}

		System.out.println(s1);
		System.out.println("Positief studieadvies: " + Studieadviseur.krijgtPositiefStudieAdvies(s1));
	}
}