/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package Studieadvies;

/**
 * Created by Melvin on 24-Feb-2014 09:35.
 */
class Student
{
	private String naam;
	private int[] cijfers;

	public Student(String naam)
	{
		this.naam = naam;
		cijfers = new int[8];
	}

	public void setCijfer(int vaknummer, int cijfer)
	{
		cijfers[vaknummer] = cijfer;
	}

	public int[] getCijfers()
	{
		return cijfers;
	}

	public String toString()
	{
		String representatie = "naam: " + naam + "\ncijfers: ";
		for (int cijfer : cijfers)
		{
			representatie += " " + cijfer;
		}
		return representatie;
	}
}
