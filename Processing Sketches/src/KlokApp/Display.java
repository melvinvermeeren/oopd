/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package KlokApp;

import processing.core.PApplet;

/**
 * Created by Melvin on 16-Feb-2014 20:21.
 */
class Display
{
	private PApplet parent;
	private int x;
	private int y;
	private DisplaySegment hours;
	private DisplaySegment minutes;

	Display(PApplet parent, int x, int y, int hoursValue, int minutesValue)
	{
		this.parent = parent;
		this.x = x;
		this.y = y;
		hours = new DisplaySegment(hoursValue, 23);
		minutes = new DisplaySegment(minutesValue, 59);
	}

	Display(PApplet parent, int x, int y)
	{
		this.parent = parent;
		this.x = x;
		this.y = y;
		hours = new DisplaySegment(23);
		minutes = new DisplaySegment(59);
	}

	void update()
	{
		if (Integer.parseInt(minutes.getValue()) == 59) hours.update();
		minutes.update();
	}

	void draw()
	{
		parent.fill(100);
		parent.rect(x, y, 65, 30);

		parent.fill(255);
		parent.text(hours.getValue() + ":" + minutes.getValue(), x + 5, y + 5);
	}
}
