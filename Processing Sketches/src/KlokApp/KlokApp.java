/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package KlokApp;

import processing.core.PApplet;

/**
 * Created by Melvin on 16-Feb-2014 20:19.
 */
public class KlokApp extends PApplet
{
	public static void main(String args[])
	{
		PApplet.main(new String[]{"KlokApp.KlokApp"});
	}

	double lastMillis;
	Display clock;

	public void setup()
	{
		size(85, 50);
		textAlign(LEFT, TOP);
		textSize(20);

		clock = new Display(this, 10, 10, 0, 0);

		background(0);
		clock.draw();

		lastMillis = millis();
	}

	public void draw()
	{
		if (millis() > lastMillis)
		{
			lastMillis = millis() + 1000;
			clock.update();

			background(0);
			clock.draw();
		}
	}
}
