/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

package KlokApp;

/**
 * Created by Melvin on 16-Feb-2014 20:21.
 */
class DisplaySegment
{
	private int value;
	private int limit;

	DisplaySegment(int value, int limit)
	{
		this.limit = limit;

		if (value >= 0 && value < this.limit) this.value = value;
		else this.value = 0;
	}

	DisplaySegment(int limit)
	{
		this.limit = limit;
		value = 0;
	}

	void setValue(int value)
	{
		if (value >= 0 && value < limit) this.value = value;
	}

	String getValue()
	{
		if (value < 10) return "0" + value;
		else return Integer.toString(value);
	}

	void update()
	{
		if (value == limit) value = 0;
		else value++;
	}
}
