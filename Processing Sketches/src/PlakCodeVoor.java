/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

/**
 * Author: Melvin.
 * Date: 04/02/14.
 */
public class PlakCodeVoor
{
	public static void main(String[] args)
	{
		String[] idLijst = {"8b3", "4bf", "9h0"};

		plakCodeVoorIdLijst(idLijst, "NL");

		for (String id : idLijst) System.out.println(id);
	}

	private static String plakCodeVoorId(String id, String code)
	{
		return code + id;
	}

	private static void plakCodeVoorIdLijst(String[] lijst, String code)
	{
		for (int i = 0; i < lijst.length; i++) lijst[i] = plakCodeVoorId(lijst[i], code);
	}
}
