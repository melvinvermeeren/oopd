/*
 * Copyright © 2014 Melvin Vermeeren. All rights reserved.
 */

import processing.core.PApplet;

/**
 * Created by Melvin on 2/6/14.
 */
class ProductMetConstructor extends PApplet
{
	public static void main(String[] args)
	{
		Product product = new Product("Pizza", 10);

		System.out.println(product.naam);
		System.out.println(product.prijs);
	}

	public static class Product
	{
		public String naam;
		public int prijs;

		public Product(String naam, int prijs)
		{
			this.naam = naam;
			this.prijs = prijs;
		}
	}
}
