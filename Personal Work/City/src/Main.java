import Game.Game;
import processing.core.PApplet;

/**
 * Created on 03-Mar-2014 20:10.
 *
 * @author Melvin
 */
public class Main extends PApplet
{
	Game game;

	public static void main(String[] args)
	{
		PApplet.main(new String[]{"--present", "Main"});
	}

	@Override
	public void setup()
	{
		frameRate(30f);
		size(displayWidth, displayHeight);
		background(0);
		textAlign(LEFT, TOP);

		game = new Game(this, 0);
	}

	@Override
	public void draw()
	{
		game.draw();
	}

	@Override
	public void mousePressed()
	{
		game.processMouseClick();
	}
}
