package Game;

/**
 * Created on 03-Mar-2014 23:51.
 *
 * @author Melvin
 */
public interface Drawable
{
	/**
	 * Draws the object on the screen.
	 */
	void draw();
}
