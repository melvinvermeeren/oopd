package Game.Character;

import Game.Clickable;
import Game.Drawable;
import Game.Game;
import Game.GameData.CharacterExpressions;
import Game.GameData.Dialogue;
import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import processing.core.PApplet;
import processing.core.PImage;

/**
 * Created on 03-Mar-2014 23:46.
 *
 * @author Melvin
 */
public class Character implements Clickable, Drawable
{
	private static PApplet parent;
	private static Game game;
	private int x, y;
	private String name;
	private PImage image;
	private Gender gender;
	private Dialogue dialogue;
	private CharacterExpressions[] characterExpressions;

	public enum Gender
	{
		MALE("Male"),
		FEMALE("Female");

		private final String gender;

		private Gender(@NotNull String gender)
		{
			this.gender = gender;
		}

		@Override
		public String toString()
		{
			return gender;
		}
	}

	public Character(@Nullable PApplet parent, @Nullable Game game, @NotNull int x, @NotNull int y, @NotNull String name, @NotNull PImage image, @NotNull Gender gender, @NotNull Dialogue dialogue, @NotNull CharacterExpressions[] characterExpressions)
	{
		if (parent != null) Character.parent = parent;
		if (game != null) Character.game = game;
		assert (Character.parent != null && Character.game != null);

		this.x = x;
		this.y = y;
		this.name = name;
		this.image = image;
		this.gender = gender;
		this.dialogue = dialogue;
		this.characterExpressions = characterExpressions;
	}

	public Character(@NotNull int x, @NotNull int y, @NotNull String name, @NotNull PImage image, @NotNull Gender gender, @NotNull Dialogue dialogue, @NotNull CharacterExpressions[] characterExpressions)
	{
		this(null, null, x, y, name, image, gender, dialogue, characterExpressions);
	}

	public String getGender()
	{
		return gender.toString();
	}

	public String getName()
	{
		return name;
	}

	public CharacterExpressions[] getCharacterExpressions()
	{
		return characterExpressions;
	}

	public Dialogue getDialogue()
	{
		return dialogue;
	}

	@Override
	public boolean isCursorOnObject()
	{
		return (parent.mouseX >= x && parent.mouseX <= x + image.width &&
				parent.mouseY >= y && parent.mouseY <= y + image.height);
	}

	@Override
	public void onClick()
	{
		if (!this.equals(game.getActiveCharacter())) game.setActiveCharacter(this);
	}

	@Override
	public void draw()
	{
		parent.image(image, x, y);
	}
}
