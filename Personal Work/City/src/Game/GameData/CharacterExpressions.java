package Game.GameData;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import processing.core.PApplet;
import processing.core.PImage;

/**
 * Created on 05-Mar-2014 18:31.
 *
 * @author Melvin
 */
public enum CharacterExpressions
{
	WHORE1_REGULAR("characters/whore1_face.png"),
	MAFIA1_REGULAR("characters/mafia1_face.png");

	private String imageFile;
	private PImage image;

	private CharacterExpressions(@Nullable String imageFile)
	{
		this.imageFile = imageFile;
	}

	public void loadImage(@NotNull PApplet parent)
	{
		image = parent.loadImage(imageFile);
		image.resize(0, Math.round(parent.height / 4f));
		imageFile = null;
	}

	@NotNull
	public PImage getImage()
	{
		return image;
	}

	public static void setup(@NotNull PApplet parent)
	{
		WHORE1_REGULAR.loadImage(parent);
		MAFIA1_REGULAR.loadImage(parent);
	}
}
