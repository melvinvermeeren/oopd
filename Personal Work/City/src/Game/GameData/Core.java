package Game.GameData;

import Game.Character.Character;
import Game.Game;
import Game.Location.Location;
import com.sun.istack.internal.NotNull;
import processing.core.PApplet;
import processing.core.PImage;

/**
 * Created on 05-Mar-2014 12:56.
 *
 * @author Melvin Vermeeren.
 */
public enum Core
{
	;

	public enum TextEngineFonts
	{
		LOCATION_REGULAR("fonts/ComicSansMS-48.vlw");

		private final String fileLocation;

		private TextEngineFonts(@NotNull String fileLocation)
		{
			this.fileLocation = fileLocation;
		}

		@Override
		public String toString()
		{
			return fileLocation;
		}
	}

	/**
	 * Loads and processes all character images.
	 *
	 * @param parent parent PApplet.
	 * @return array containing all character images.
	 */
	public static PImage[] getCharacterImages(PApplet parent)
	{
		PImage[] characterImages =
				{
						parent.loadImage("characters/whore1.png"),
						parent.loadImage("characters/mafia1.png")
				};

		characterImages[0].resize(Math.round(parent.width / 8.5f), 0);
		characterImages[1].resize(Math.round(parent.width / 30f), 0);

		return characterImages;
	}

	/**
	 * Loads and processes all location images.
	 *
	 * @param parent parent PApplet.
	 * @return array containing all character images.
	 */
	public static PImage[] getLocationImages(PApplet parent)
	{
		PImage[] locationImages =
				{
						parent.loadImage("locations/emptyStreet.jpg")
				};

		locationImages[0].resize(parent.width, parent.height);

		return locationImages;
	}

	/**
	 * Loads all game characters.
	 *
	 * @param parent          parent PApplet.
	 * @param game            parent Game.
	 * @param characterImages array containing all character images.
	 * @return array containing all characters.
	 */
	public static Character[] getCharacters(PApplet parent, Game game, PImage[] characterImages)
	{
		return new Character[]
				{
						new Character(parent, game, Math.round(parent.width / 20f), Math.round(parent.height / 2f), "Hoerfag", characterImages[0], Character.Gender.FEMALE, Dialogue.WHORE1, whore1_expressions()),
						new Character(parent.width - Math.round(parent.width / 20f), parent.height - Math.round(parent.height / 2.18f), "Mafia Weed™", characterImages[1], Character.Gender.MALE, Dialogue.MAFIA1, mafia1_expressions())
				};
	}

	/**
	 * Loads all game locations.
	 *
	 * @param parent         parent PApplet.
	 * @param game           parent Game.
	 * @param locationImages array containing all location images.
	 * @param characters     array containing all characters.
	 * @return array containing all locations.
	 */
	public static Location[] getLocations(PApplet parent, Game game, PImage[] locationImages, Character[] characters)
	{
		return new Location[]
				{
						new Location(parent, game, 0, 0, "St. Peterstreet", locationImages[0], characters)
				};
	}

	private static CharacterExpressions[] whore1_expressions()
	{
		return new CharacterExpressions[]
				{
						CharacterExpressions.WHORE1_REGULAR
				};
	}

	private static CharacterExpressions[] mafia1_expressions()
	{
		return new CharacterExpressions[]
				{
						CharacterExpressions.MAFIA1_REGULAR
				};
	}
}
