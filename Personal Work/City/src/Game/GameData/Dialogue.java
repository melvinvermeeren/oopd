package Game.GameData;

/**
 * Created on 05-Mar-2014 18:02.
 *
 * @author Melvin
 */
public enum Dialogue
{
	WHORE1(new String[]
			{
					"Kom neuken schatje!",
					"Ik wil eerst GELD. xxx~",
					"Bovendien is mijn hond echt heel cool!\nEcht super sexy is dat beest schatje."
			}),
	MAFIA1(new String[]
			{
					"Wil je wiet kopen?",
					"Is héél goeie wiet G!",
					"Ik heb trouwens ook nog coke enzo, echt goed spul.\nAls je het gebruikt flip je 'm helemaal!\nHahaha!",
					"Die hoer heeft trouwens aids, dus pas op."
			});

	private final String[] dialogue;
	private static int currentIndex;

	private Dialogue(String[] dialogue)
	{
		this.dialogue = dialogue;
		resetDialoguePostion();
	}

	public String getCurrentDialogue()
	{
		return dialogue[currentIndex];
	}

	public static void incrementDialogue(Game.Character.Character character)
	{
		if (currentIndex < character.getDialogue().dialogue.length - 1) currentIndex++;
	}

	public static void decrementDialogue()
	{
		if (currentIndex > 0) currentIndex--;
	}

	public static void resetDialoguePostion()
	{
		currentIndex = 0;
	}
}
