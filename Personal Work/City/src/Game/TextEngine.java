package Game;

import Game.Character.Character;
import Game.GameData.Core;
import Game.GameData.Dialogue;
import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PImage;

/**
 * Created on 05-Mar-2014 13:30.
 *
 * @author Melvin
 */
public class TextEngine implements Clickable
{
	private static PApplet parent;
	private static Game game;
	private PFont font;

	public TextEngine(@Nullable PApplet parent, @Nullable Game game, @NotNull Core.TextEngineFonts font)
	{
		if (TextEngine.parent == null) TextEngine.parent = parent;
		if (TextEngine.game == null) TextEngine.game = game;
		assert (TextEngine.parent != null && TextEngine.game != null);

		this.font = parent.loadFont(font.toString());
	}

	public TextEngine(@NotNull Core.TextEngineFonts font)
	{
		this(null, null, font);
	}

	public void text(@NotNull String text, @NotNull int fontSize, @NotNull int color, @NotNull int x, @NotNull int y)
	{
		parent.fill(color);
		parent.textFont(font, fontSize);
		parent.text(text, x, y);
	}

	public static void textBox(@NotNull Character character)
	{
		parent.textSize(Math.round(parent.width / 75f));
		parent.fill(0xAA0000FF);

		int heightMargin = Math.round(parent.height / 3.5f);
		parent.rect(0, parent.height - heightMargin, parent.width, parent.height - heightMargin);

		PImage image = character.getCharacterExpressions()[0].getImage();
		parent.image(image, 10, parent.height - image.height);

		parent.fill(0xFFFFFFFF);
		parent.text(character.getName() + "   -   " + character.getGender().toString() + "\n\n" + character.getDialogue().getCurrentDialogue(), image.width + 20, parent.height - image.height, parent.width - 10, parent.height - 10);
	}

	@Override
	public boolean isCursorOnObject()
	{
		return (parent.mouseY > parent.height - Math.round(parent.height / 3.5f));
	}

	@Override
	public void onClick()
	{
		Dialogue.incrementDialogue(game.getActiveCharacter());
	}
}
