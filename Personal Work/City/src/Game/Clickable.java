package Game;

/**
 * Created on 04-Mar-2014 00:06.
 *
 * @author Melvin
 */
public interface Clickable
{
	/**
	 * Checks if the cursor is currently on this object.
	 *
	 * @return <ul><li>{@code true} if cursor is on this object.</li><li>{@code false} if cursor is <b>not</b> on this object.</li></ul>
	 */
	public boolean isCursorOnObject();

	/**
	 * Process mouse click.
	 */
	public void onClick();
}
