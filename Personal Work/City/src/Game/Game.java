package Game;

import Game.Character.Character;
import Game.GameData.CharacterExpressions;
import Game.GameData.Core;
import Game.Location.Location;
import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import processing.core.PApplet;
import processing.core.PImage;

/**
 * Created on 03-Mar-2014 23:02.
 *
 * @author Melvin
 */
public class Game implements Drawable
{
	private Location[] locations;
	private Location currentLocation;
	private Character activeCharacter;

	public static Location[] loadGame(@NotNull PApplet parent, @NotNull Game game)
	{
		CharacterExpressions.setup(parent);
		PImage characterImages[] = Core.getCharacterImages(parent);
		PImage locationImages[] = Core.getLocationImages(parent);
		Character[] characters = Core.getCharacters(parent, game, characterImages);
		return Core.getLocations(parent, game, locationImages, characters);
	}

	public Game(@NotNull PApplet parent, @NotNull int startLocationIndex)
	{
		locations = loadGame(parent, this);

		if (startLocationIndex > locations.length - 1)
			throw new IllegalArgumentException("startLocationIndex must be <= locations.length.");

		currentLocation = locations[startLocationIndex];
	}

	public void setCurrentLocation(@NotNull Location newLocation)
	{
		this.currentLocation = newLocation;
	}

	public Character getActiveCharacter()
	{
		return activeCharacter;
	}

	public void setActiveCharacter(@Nullable Character activeCharacter)
	{
		this.activeCharacter = activeCharacter;
	}

	public void processMouseClick()
	{
		currentLocation.processMouseClick();
	}

	@Override
	public void draw()
	{
		/* Draw main screen. */
		currentLocation.draw();

		/* Draw TextEngine overlay. */
		if (activeCharacter != null) TextEngine.textBox(activeCharacter);
	}
}
