package Game.Location;

import Game.Clickable;
import Game.Game;
import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import processing.core.PApplet;

/**
 * Created on 04-Mar-2014 15:06.
 *
 * @author Melvin
 */
public class Exit implements Clickable
{
	private static PApplet parent;
	private static Game game;
	private int x, y, width, height;
	private Location toLocation;

	public Exit(@Nullable PApplet parent, @Nullable Game game, @NotNull int x, @NotNull int y, @NotNull int width, @NotNull int height, @NotNull Location toLocation)
	{
		if (parent != null) Exit.parent = parent;
		if (game != null) Exit.game = game;
		assert (Exit.parent != null && Exit.game != null);

		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.toLocation = toLocation;
	}

	public Exit(@NotNull int x, @NotNull int y, @NotNull int width, @NotNull int height, @NotNull Location toLocation)
	{
		this(null, null, x, y, width, height, toLocation);
	}

	@Override
	public boolean isCursorOnObject()
	{
		return (parent.mouseX >= x && parent.mouseX <= x + width &&
				parent.mouseY >= y && parent.mouseY <= y + height);
	}

	@Override
	public void onClick()
	{
		game.setActiveCharacter(null);
		game.setCurrentLocation(toLocation);
	}
}
