package Game.Location;

import Game.Character.Character;
import Game.Drawable;
import Game.Game;
import Game.GameData.Core.TextEngineFonts;
import Game.GameData.Dialogue;
import Game.TextEngine;
import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import processing.core.PApplet;
import processing.core.PImage;

/**
 * Created on 03-Mar-2014 23:01.
 *
 * @author Melvin
 */
public class Location implements Drawable
{
	private static PApplet parent;
	private static Game game;
	private static TextEngine textEngine;
	private static final int TEXT_COLOR = 0xFFFFFFFF;
	private int x, y;
	private Exit[] exits;
	private Character[] characters;
	private String name;
	private PImage background;

	public Location(@Nullable PApplet parent, @Nullable Game game, @NotNull int x, @NotNull int y, @NotNull String name, @NotNull PImage image, @Nullable Character[] characters)
	{
		if (parent != null) Location.parent = parent;
		if (game != null) Location.game = game;
		assert (Location.parent != null && Location.game != null);

		if (textEngine == null) textEngine = new TextEngine(parent, game, TextEngineFonts.LOCATION_REGULAR);

		this.x = x;
		this.y = y;
		this.characters = characters;
		this.name = name;
		this.background = image;
	}

	public Location(@NotNull int x, @NotNull int y, @NotNull String name, @NotNull PImage image, @Nullable Character[] characters)
	{
		this(null, null, x, y, name, image, characters);
	}

	public void processMouseClick()
	{
		for (Character character : characters)
		{
			if (character.isCursorOnObject() && !character.equals(game.getActiveCharacter()))
			{
				character.onClick();
				Dialogue.resetDialoguePostion();
				return;
			}
		}

		if (textEngine.isCursorOnObject())
		{
			textEngine.onClick();
			return;
		}

		/* No click on anything, reset all to null. */
		game.setActiveCharacter(null);
	}

	@Override
	public void draw()
	{
		parent.image(background, 0, 0);

		for (Character character : characters)
		{
			character.draw();
		}

		textEngine.text(name, Math.round(parent.width / 50f), TEXT_COLOR, 10, 10);
	}
}
