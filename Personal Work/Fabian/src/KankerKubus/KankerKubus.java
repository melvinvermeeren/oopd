package KankerKubus;

import processing.core.PApplet;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created on 29-Apr-2014 20:31.
 *
 * @author Melvin
 */
public class KankerKubus extends PApplet
{
	public static final int BACKGROUND_COLOR = 0x30000000;
	public static final int NUMBER_OF_VIERKANTAI = 10;

	private Vierkant vierkant;
	private ArrayList<VierkantAI> vierkantAIs;
	private ArrayList<Character> inputKeys;

	public static void main(String[] args)
	{
		PApplet.main(new String[]{"--present", "KankerKubus.KankerKubus"});
	}

	@Override
	public void setup()
	{
		size(displayWidth, displayHeight);
		frameRate(60f);

		vierkant = new Vierkant(this, 10, 10);

		vierkantAIs = new ArrayList<>();
		Random random = new Random();
		for (int i = 0; i < NUMBER_OF_VIERKANTAI; i++)
		{
			vierkantAIs.add(new VierkantAI(this, random.nextInt(width + 1 - Vierkant.SIZE), random.nextInt(height + 1 - Vierkant.SIZE)));
		}
		inputKeys = new ArrayList<>();

		background(Vierkant.COLOR);
	}

	@Override
	public void draw()
	{
		/* Update */
		for (char key : inputKeys)
		{
			switch (key)
			{
				case 'w':
					vierkant.move(Vierkant.Direction.UP);
					break;
				case 's':
					vierkant.move(Vierkant.Direction.DOWN);
					break;
				case 'a':
					vierkant.move(Vierkant.Direction.LEFT);
					break;
				case 'd':
					vierkant.move(Vierkant.Direction.RIGHT);
					break;
			}
		}
		for (VierkantAI vierkantAI : vierkantAIs)
			vierkantAI.act();

		/* Draw. */
		fill(BACKGROUND_COLOR);
		rect(0, 0, width, height);

		vierkant.draw();
		for (VierkantAI vierkantAI : vierkantAIs)
			vierkantAI.draw();
	}

	@Override
	public void keyPressed()
	{
		if (key == 'w' || key == 's' || key == 'a' || key == 'd')
		{
			for (char key : inputKeys)
				if (key == this.key)
					return;

			inputKeys.add((Character) key);
		}
	}

	@Override
	public void keyReleased()
	{
		if (key == 'w' || key == 's' || key == 'a' || key == 'd')
			inputKeys.remove((Character) key);
	}
}
