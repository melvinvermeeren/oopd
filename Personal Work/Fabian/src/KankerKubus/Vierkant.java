package KankerKubus;

import processing.core.PApplet;

/**
 * Created on 29-Apr-2014 20:35.
 *
 * @author Melvin
 */
public class Vierkant
{
	public static final int SIZE = 50;
	public static final int MOVEMENT_SPEED = 10;
	public static final int COLOR = 0xFFFF0000;
	private static PApplet parent;
	private int x, y;

	public Vierkant(PApplet parent, int x, int y)
	{
		if (Vierkant.parent == null) Vierkant.parent = parent;
		this.x = x;
		this.y = y;
	}

	public void draw()
	{
		parent.fill(COLOR);
		parent.rect(x, y, SIZE, SIZE);
	}

	public boolean move(Direction direction)
	{
		switch (direction)
		{
			case UP:
				y -= MOVEMENT_SPEED;
				if (y < 0)
				{
					y = 0;
					return true;
				}
				break;
			case DOWN:
				y += MOVEMENT_SPEED;
				if (y > parent.height - SIZE)
				{
					y = parent.height - SIZE;
					return true;
				}
				break;
			case LEFT:
				x -= MOVEMENT_SPEED;
				if (x < 0)
				{
					x = 0;
					return true;
				}
				break;
			case RIGHT:
				x += MOVEMENT_SPEED;
				if (x > parent.width - SIZE)
				{
					x = parent.width - SIZE;
					return true;
				}
				break;
		}
		return false;
	}

	public static enum Direction
	{
		UP,
		DOWN,
		LEFT,
		RIGHT
	}
}
