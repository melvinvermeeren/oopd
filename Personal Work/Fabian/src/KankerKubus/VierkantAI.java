package KankerKubus;

import processing.core.PApplet;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created on 30-Apr-2014 15:20.
 *
 * @author Melvin
 */
public class VierkantAI extends Vierkant
{
	private Direction direction;
	private Random random;
	private Timer timer;

	public VierkantAI(PApplet parent, int x, int y)
	{
		super(parent, x, y);

		random = new Random();

		timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask()
		{
			@Override
			public void run()
			{
				updateDirection();
			}
		}
		, 0, 250);
	}

	public void act()
	{
		if (direction != null)
			if (move(direction))
				updateDirection();
	}

	private void updateDirection()
	{
		switch (random.nextInt(5))
		{
			case 0:
				direction = Direction.UP;
				break;
			case 1:
				direction = Direction.DOWN;
				break;
			case 2:
				direction = Direction.LEFT;
				break;
			case 3:
				direction = Direction.RIGHT;
				break;
			case 4:
				direction = null;
				break;
		}
	}
}
