package Space_Shooter;

import processing.core.PApplet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/**
 * Created on 10-Mar-2014 12:46.
 *
 * @author Melvin
 */
public class SpaceShooter extends PApplet
{
	public static final int NUMBER_OF_ENEMYSHIPS = 2;

	private static ArrayList<Drawable> drawables;
	private static ArrayList<Laser> lasers;

	PlayerShip playerShip;
	EnemyShip[] enemyShips;

	public static void main(String[] args)
	{
		PApplet.main(new String[]{"--present", "Space_Shooter.SpaceShooter"});
	}

	public void setup()
	{
		size(displayWidth, displayHeight);

		Spaceship.setup(this);
		PlayerShip.setup(this);
		EnemyShip.setup(this);
		Laser.setup(this);

		drawables = new ArrayList<>();
		lasers = new ArrayList<>();

		playerShip = new PlayerShip(Direction.UP, displayWidth / 2, displayHeight - PlayerShip.SIZE * 2);

		enemyShips = new EnemyShip[NUMBER_OF_ENEMYSHIPS];
		for (int i = 0; i < enemyShips.length; i++)
		{
			enemyShips[i] = new EnemyShip(Direction.DOWN, displayWidth / 2, PlayerShip.SIZE * 2);
			delay(10); // Wait a bit to get decent random seeds.
		}

		drawables.add(playerShip);
		Collections.addAll(drawables, enemyShips);
	}

	public void draw()
	{
		background(0);

		/* Update stuff. */
		playerShip.update();
		for (EnemyShip enemyShip : enemyShips) enemyShip.update();

		Iterator iterator = lasers.iterator();
		Laser laser;

		while(iterator.hasNext())
		{
			laser = (Laser)iterator.next();

			laser.update();
			if (laser.isOutsideCanvas())
			{
				iterator.remove();
				drawables.remove(laser);
			}

			if (playerShip.collidingWithLaser(laser))
			{
				background(0xAAAA0000);
			}
			for (int i = 0; i < enemyShips.length; i++)
			{
				if (enemyShips[i].collidingWithLaser(laser))
				{
					EnemyShip newShip = new EnemyShip(Direction.DOWN, displayWidth / 2, PlayerShip.SIZE * 2);
					drawables.set(drawables.indexOf(enemyShips[i]), newShip);
					enemyShips[i] = newShip;
					background(0xAA00AA00);
				}
			}
		}

		/* Draw stuff. */
		for (Drawable drawable : drawables)
		{
			drawable.draw();
		}
	}

	public static void addLaser(Laser laser)
	{
		lasers.add(laser);
		drawables.add(laser);
	}
}
