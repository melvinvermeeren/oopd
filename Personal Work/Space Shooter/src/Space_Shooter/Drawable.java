package Space_Shooter;

/**
 * Created on 26-Mar-2014 19:15.
 *
 * @author Melvin
 */
public interface Drawable
{
	void draw();
}
