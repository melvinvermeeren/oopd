package Space_Shooter;

import processing.core.PApplet;

/**
 * Created on 26-Mar-2014 18:28.
 *
 * @author Melvin
 */
public abstract class Spaceship implements Drawable
{
	public static final int BURST_COOLDOWN = 500;
	public static final int BURST_DELAY = 0;
	public static final int BURST_SIZE = 25;
	public static final int SPEED_X = 20;
	public static final int SPEED_Y = 0;
	public static final int SIZE = 30;

	protected static PApplet parent;
	protected Direction direction;

	protected int x;
	protected int y;
	protected int burstPosition;
	protected long cooldownTimer;

	public static void setup(PApplet parent)
	{
		Spaceship.parent = parent;
	}

	protected void shoot()
	{
		SpaceShooter.addLaser(new Laser(direction, x, y));
	}

	public void update()
	{
		if (parent.millis() >= cooldownTimer)
		{
			if (burstPosition < BURST_SIZE)
			{
				cooldownTimer = parent.millis() + BURST_DELAY;
				burstPosition++;
			}
			else
			{
				cooldownTimer = parent.millis() + BURST_COOLDOWN;
				burstPosition = 0;
			}
			shoot();
		}
	}

	@Override
	public void draw()
	{
		switch (direction)
		{
			case UP:
				parent.triangle(x, y - SIZE * 2, x + SIZE, y, x - SIZE, y);
				break;

			case DOWN:
				parent.triangle(x, y + SIZE * 2, x + SIZE, y, x - SIZE, y);
				break;

			case LEFT:
				parent.triangle(x - SIZE * 2, y, x, y + SIZE, x, y - SIZE);
				break;

			case RIGHT:
				parent.triangle(x + SIZE * 2, y, x, y + SIZE, x, y - SIZE);
				break;
		}
	}

	protected void move(Direction direction)
	{
		if (!isOutsideCanvasOnDirection(direction))
		{
			switch (direction)
			{
				case LEFT:
					x -= SPEED_X;
					break;

				case RIGHT:
					x += SPEED_X;
					break;

				default:
					break;
			}
		}
	}

	protected boolean isOutsideCanvasOnDirection(Direction direction)
	{
		switch (direction)
		{
			case LEFT:
				if (x - SPEED_X <= SIZE)
				{
					x = SIZE;
					return true;
				}
				break;

			case RIGHT:
				if (x + SPEED_X >= parent.displayWidth - SIZE)
				{
					x = parent.displayWidth - SIZE;
					return true;
				}
				break;

			default:
				break;
		}

		return false;
	}

	protected boolean collidingWithLaser(Laser laser)
	{
		return x - SIZE <= laser.getX() && x + SIZE >= laser.getX() &&
				y - SIZE <= laser.getY() && y + SIZE >= laser.getY();
	}
}
