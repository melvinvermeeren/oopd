package Space_Shooter;

import java.util.Random;

/**
 * Created on 26-Mar-2014 20:13.
 *
 * @author Melvin
 */
public final class EnemyShip extends Spaceship
{
	public static final int MOVEMENT_REFRESH_MIN = 100;
	public static final int MOVEMENT_REFRESH_MAX = 1000;
	public static final int RANDOM_SHOOT_DIFF = 100;

	private long movementTimer;
	private Direction movementDirection;
	private Random random;

	public EnemyShip(Direction direction, int x, int y)
	{
		this.direction = direction;
		this.x = x;
		this.y = y;
		this.cooldownTimer = 0;
		this.movementTimer = 0;
		this.burstPosition = 0;
		this.random = new Random();
	}

	@Override
	public void draw()
	{
		parent.fill(0xFFFF0000);

		super.draw();
	}

	@Override
	public void update()
	{
		cooldownTimer += random.nextInt(RANDOM_SHOOT_DIFF + 1) - RANDOM_SHOOT_DIFF / 2;
		super.update();

		act();
	}

	public void act()
	{
		if (parent.millis() >= movementTimer)
		{
			movementTimer = parent.millis() + random.nextInt(MOVEMENT_REFRESH_MAX - MOVEMENT_REFRESH_MIN + 1) + MOVEMENT_REFRESH_MIN;

			switch (random.nextInt(2))
			{
				case 0:
					movementDirection = Direction.LEFT;
					break;

				case 1:
					movementDirection = Direction.RIGHT;
					break;
			}
		}

		if (isOutsideCanvasOnDirection(movementDirection))
		{
			movementDirection = Direction.invertDirection(movementDirection);
		}

		move(movementDirection);
	}
}
