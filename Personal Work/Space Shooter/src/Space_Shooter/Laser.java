package Space_Shooter;

import processing.core.PApplet;

/**
 * Created on 26-Mar-2014 18:48.
 *
 * @author Melvin
 */
public class Laser implements Drawable
{
	public static final int SPEED = 40;
	public static final int COLOUR = 0xFFFF0000;
	public static final int LASER_LENGTH = 30;
	public static final int LASER_WIDTH = 5;

	private static PApplet parent;
	private Direction direction;
	private int x;
	private int y;

	public static void setup(PApplet parent)
	{
		Laser.parent = parent;
	}

	public Laser(Direction direction, int x, int y)
	{
		this.direction = direction;
		this.x = x;
		this.y = y;

		createDistance();
	}

	private void createDistance()
	{
		switch(direction)
		{
			case UP:
				y -= Spaceship.SIZE * 2 + LASER_LENGTH;
				break;

			case DOWN:
				y += Spaceship.SIZE * 2 + LASER_LENGTH;
				break;

			case LEFT:
				x -= Spaceship.SIZE * 2 + LASER_LENGTH;
				break;

			case RIGHT:
				x += Spaceship.SIZE * 2 + LASER_LENGTH;
				break;
		}
	}

	public void update()
	{
		switch(direction)
		{
			case UP:
				y -= SPEED;
				break;

			case DOWN:
				y += SPEED;
				break;

			case LEFT:
				x -= SPEED;
				break;

			case RIGHT:
				x += SPEED;
				break;
		}
	}

	public boolean isOutsideCanvas()
	{
		return x < 0 || x > parent.displayWidth ||
				y < 0 || y > parent.displayHeight;
	}

	@Override
	public void draw()
	{
		parent.fill(COLOUR);
		parent.rect(x, y, LASER_WIDTH, LASER_LENGTH, 1);
	}

	public int getX()
	{
		return x;
	}

	public int getY()
	{
		return y;
	}
}
