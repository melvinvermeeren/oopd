package Space_Shooter;

/**
 * Created on 26-Mar-2014 19:22.
 *
 * @author Melvin
 */
public final class PlayerShip extends Spaceship
{
	public PlayerShip(Direction direction, int x, int y)
	{
		this.direction = direction;
		this.x = x;
		this.y = y;
		this.cooldownTimer = 0;
		this.burstPosition = 0;
	}

	@Override
	public void update()
	{
		super.update();

		if (!parent.keyPressed) return;

		switch (parent.key)
		{
			case 'a':
				move(Direction.LEFT);
				break;

			case 'd':
				move(Direction.RIGHT);
				break;

			default:
				break;
		}
	}

	@Override
	public void draw()
	{
		parent.fill(0xFF00FF00);
		super.draw();
	}
}
