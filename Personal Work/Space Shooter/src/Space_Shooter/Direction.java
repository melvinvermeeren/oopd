package Space_Shooter;

/**
 * Created on 26-Mar-2014 18:49.
 *
 * @author Melvin
 */
public enum Direction
{
	UP,
	DOWN,
	LEFT,
	RIGHT;

	public static Direction invertDirection(Direction direction)
	{
		switch (direction)
		{
			case LEFT:
				return RIGHT;

			default:
				return LEFT;
		}
	}
}
