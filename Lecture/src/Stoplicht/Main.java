package Stoplicht;

import processing.core.PApplet;

/**
 * Created on 23-Apr-2014 16:42.
 *
 * @author Melvin
 */
public class Main extends PApplet
{
	Stoplicht stoplichtLinks;

	public static void main(String[] args)
	{
		PApplet.main(new String[] { "--present", "Stoplicht.Main" });
	}

	@Override
	public void setup()
	{
		Licht.setup(this);
		size(displayWidth, displayHeight);
		noStroke();

		stoplichtLinks = new Stoplicht(displayWidth / 2 - displayWidth / 4, displayHeight / 2);
		stoplichtLinks.actieveLicht(Stoplicht.LICHT_BOVEN);
	}

	@Override
	public void draw()
	{
		background(0);
		stoplichtLinks.draw();
	}
}
