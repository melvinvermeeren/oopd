package Stoplicht;

import processing.core.PApplet;

/**
 * Created on 23-Apr-2014 16:43.
 *
 * @author Melvin
 */
public class Licht
{
	public static final int GROOTE = 50;

	private static PApplet parent;

	private int x;
	private int y;
	private int kleurAan;
	private int kleurUit;
	private boolean aan;

	public Licht(int x, int y, int kleurAan, int kleurUit)
	{
		this.x = x;
		this.y = y;
		this.kleurAan = kleurAan;
		this.kleurUit = kleurUit;
		aan = false;
	}

	public static void setup(PApplet parent)
	{
		Licht.parent = parent;
	}

	public void aan(boolean aan)
	{
		this.aan = aan;
	}

	public void draw()
	{
		parent.fill(aan ? kleurAan : kleurUit);
		parent.ellipse(x, y, GROOTE, GROOTE);
	}
}
