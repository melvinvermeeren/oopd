package Stoplicht;

/**
 * Created on 23-Apr-2014 17:10.
 *
 * @author Melvin
 */
public class Stoplicht
{
	public static final int LICHT_BOVEN = 0;
	public static final int LICHT_MIDDEN = 1;
	public static final int LICHT_ONDER = 2;
	public static final int LICHT_AFSTAND = 75;

	private Licht[] lichten;
	private int x;
	private int y;

	public Stoplicht(int x, int y)
	{
		this.lichten = new Licht[]
				{
						new Licht(x, y - LICHT_AFSTAND, 0xFFFF0000, 0xFF330000),
						new Licht(x, y, 0xFFFFFF00, 0xFF333300),
						new Licht(x, y + LICHT_AFSTAND, 0xFF00FF00, 0xFF003300)
				};
		this.x = x;
		this.y = y;
	}

	public void draw()
	{
		for (Licht licht : lichten)
			licht.draw();
	}

	public void actieveLicht(int lichtPositie)
	{
		for (int i = 0; i < lichten.length; i++)
		{
			lichten[i].aan(i == lichtPositie);
		}
	}
}
